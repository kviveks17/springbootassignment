package com.admbootup.spring;

import static com.admbootup.spring.util.CITIES.AHEMDABAD;
import static com.admbootup.spring.util.CITIES.BENGALURU;
import static com.admbootup.spring.util.CITIES.CHENNAI;
import static com.admbootup.spring.util.CITIES.DELHI;
import static com.admbootup.spring.util.CITIES.HYDERABAD;
import static com.admbootup.spring.util.CITIES.KOLKATA;
import static com.admbootup.spring.util.CITIES.MANGALORE;
import static com.admbootup.spring.util.CITIES.MUMBAI;
import static com.admbootup.spring.util.CITIES.PORTBLAIR;
import static com.admbootup.spring.util.CITIES.PUNE;
import static com.admbootup.spring.util.FlightType.AIRBUS;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.admbootup.spring.entity.AirlineInfo;
import com.admbootup.spring.entity.Fare;
import com.admbootup.spring.entity.Flight;
import com.admbootup.spring.entity.FlightInfo;
import com.admbootup.spring.entity.Passenger;
import com.admbootup.spring.service.AirlineInfoService;
import com.admbootup.spring.service.FlightInfoService;
import com.admbootup.spring.service.FlightService;
import com.admbootup.spring.service.PassengerService;

@SpringBootApplication
public class SringbootassignmentApplication {

	@Autowired
	private FlightService flightService;

	@Autowired
	private FlightInfoService flightInfoService;

	@Autowired
	private AirlineInfoService airlineService;

	@Autowired
	private PassengerService passengerService;

	@PostConstruct
	public void init() throws ParseException {
		prepareDBTestRecords();
	}

	private void prepareDBTestRecords() {
		createAirlineRecords();
		createFlightInformation();
//		createFlightRecords();
		createPassengerRecords();
	}

	private void createPassengerRecords() {
		Passenger p1 = new Passenger("Praveen", "Reddy", "Mr.", "praveen@abc.com", "1234567890", "Male",
				"Praveen" + "123");
		Passenger p2 = new Passenger("James", "Goedic", "Mr.", "james@abc.com", "1234568790", "Male", "James" + "123");
		passengerService.saveOrUpdateAll(Arrays.asList(p1, p2));
	}

	private void createFlightInformation() {

		AirlineInfo aiAirIndia = airlineService.getAirlineInfoByName("Air India");
		AirlineInfo aiIndigo = airlineService.getAirlineInfoByName("Indigo");
		AirlineInfo aiAirAsia = airlineService.getAirlineInfoByName("Air Asia");
		AirlineInfo aiVistara = airlineService.getAirlineInfoByName("Vistara");
		AirlineInfo aiTrueJet = airlineService.getAirlineInfoByName("True Jet");
		AirlineInfo aiGoAir = airlineService.getAirlineInfoByName("Go Air");

		FlightInfo fi1 = new FlightInfo("AI-840", AIRBUS, aiAirIndia);
		FlightInfo fi2 = new FlightInfo("AI-850", AIRBUS, aiAirIndia);
		FlightInfo fi3 = new FlightInfo("AI-860", AIRBUS, aiAirIndia);
		FlightInfo fi4 = new FlightInfo("AI-870", AIRBUS, aiAirIndia);
		FlightInfo fi5 = new FlightInfo("6E-6684", AIRBUS, aiIndigo);
		FlightInfo fi6 = new FlightInfo("6E-6685", AIRBUS, aiIndigo);
		FlightInfo fi7 = new FlightInfo("6E-6686", AIRBUS, aiIndigo);
		FlightInfo fi8 = new FlightInfo("6E-6687", AIRBUS, aiIndigo);
		FlightInfo fi9 = new FlightInfo("I5-755", AIRBUS, aiAirAsia);
		FlightInfo fi10 = new FlightInfo("I5-756", AIRBUS, aiAirAsia);
		FlightInfo fi11 = new FlightInfo("I5-757", AIRBUS, aiAirAsia);
		FlightInfo fi12 = new FlightInfo("I5-758", AIRBUS, aiAirAsia);
		FlightInfo fi13 = new FlightInfo("UK-830", AIRBUS, aiVistara);
		FlightInfo fi14 = new FlightInfo("UK-831", AIRBUS, aiVistara);
		FlightInfo fi15 = new FlightInfo("UK-832", AIRBUS, aiVistara);
		FlightInfo fi16 = new FlightInfo("UK-833", AIRBUS, aiVistara);
		FlightInfo fi17 = new FlightInfo("2T-518", AIRBUS, aiTrueJet);
		FlightInfo fi18 = new FlightInfo("2T-519", AIRBUS, aiTrueJet);
		FlightInfo fi19 = new FlightInfo("2T-520", AIRBUS, aiTrueJet);
		FlightInfo fi20 = new FlightInfo("2T-521", AIRBUS, aiTrueJet);
		FlightInfo fi21 = new FlightInfo("G8-424", AIRBUS, aiGoAir);
		FlightInfo fi22 = new FlightInfo("G8-425", AIRBUS, aiGoAir);
		FlightInfo fi23 = new FlightInfo("G8-427", AIRBUS, aiGoAir);
		FlightInfo fi24 = new FlightInfo("G8-428", AIRBUS, aiGoAir);
		FlightInfo fi25 = new FlightInfo("G8-450", AIRBUS, aiGoAir);
		FlightInfo fi26 = new FlightInfo("SG-432", AIRBUS, aiGoAir);
		FlightInfo fi27 = new FlightInfo("SG-433", AIRBUS, aiGoAir);
		FlightInfo fi28 = new FlightInfo("SG-434", AIRBUS, aiGoAir);
		FlightInfo fi29 = new FlightInfo("SG-435", AIRBUS, aiGoAir);

		flightInfoService.updateOrSaveAll(Arrays.asList(fi1, fi2, fi3, fi4, fi5, fi6, fi7, fi8, fi9, fi10, fi11, fi12,
				fi13, fi14, fi15, fi16, fi17, fi18, fi19, fi20, fi21, fi22, fi23, fi24, fi25, fi26, fi27, fi28, fi29));
	}

	private void createFlightRecords() {

		FlightInfo ai840 = flightInfoService.getByName("AI-840");
		FlightInfo ai850 = flightInfoService.getByName("AI-850");
		FlightInfo ai860 = flightInfoService.getByName("AI-860");
		FlightInfo ai870 = flightInfoService.getByName("AI-870");
		FlightInfo f6e6684 = flightInfoService.getByName("6E-6684");
		FlightInfo f6e6685 = flightInfoService.getByName("6E-6685");
		FlightInfo f6e6686 = flightInfoService.getByName("6E-6686");
		FlightInfo f6e6687 = flightInfoService.getByName("6E-6687");
		FlightInfo fI5755 = flightInfoService.getByName("I5-755");
		FlightInfo fI5756 = flightInfoService.getByName("I5-756");
		FlightInfo fI5757 = flightInfoService.getByName("I5-757");
		FlightInfo fSG434 = flightInfoService.getByName("SG-434");
		FlightInfo fSG435 = flightInfoService.getByName("SG-435");
		FlightInfo fUK830 = flightInfoService.getByName("UK-830");
		FlightInfo fUK831 = flightInfoService.getByName("UK-831");
		FlightInfo fUK832 = flightInfoService.getByName("UK-832");
		FlightInfo fUK833 = flightInfoService.getByName("UK-833");
		FlightInfo f2T518 = flightInfoService.getByName("2T-518");
		FlightInfo f2T519 = flightInfoService.getByName("2T-519");

		Flight f1 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(2, 12),
				DELHI.name(), new Fare("INR", 4500D), ai840);

		Flight f2 = new Flight(HYDERABAD.name(), "2 hours 45 mins", LocalDate.of(2020, 8, 21), LocalTime.of(1, 15),
				DELHI.name(), new Fare("INR", 3500D), ai850);

		Flight f3 = new Flight(MUMBAI.name(), "2 hours 50 mins", LocalDate.of(2020, 8, 21), LocalTime.of(2, 30),
				CHENNAI.name(), new Fare("INR", 5000D), ai860);

		Flight f4 = new Flight(HYDERABAD.name(), "1 hours 45 mins", LocalDate.of(2020, 8, 21), LocalTime.of(2, 45),
				PUNE.name(), new Fare("INR", 2546D), ai870);

		Flight f5 = new Flight(PORTBLAIR.name(), "3 hours 00 mins", LocalDate.of(2020, 8, 21), LocalTime.of(3, 00),
				CHENNAI.name(), new Fare("INR", 7500D), f6e6684);

		Flight f6 = new Flight(DELHI.name(), "3 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(3, 15),
				BENGALURU.name(), new Fare("INR", 10000D), f6e6685);

		Flight f7 = new Flight(PUNE.name(), "4 hours 07 mins", LocalDate.of(2020, 8, 21), LocalTime.of(3, 15),
				DELHI.name(), new Fare("INR", 7438D), f6e6686);

		Flight f8 = new Flight(MANGALORE.name(), "3 hours 30 mins", LocalDate.of(2020, 8, 21), LocalTime.of(3, 15),
				HYDERABAD.name(), new Fare("INR", 8743D), f6e6687);

		Flight f9 = new Flight(AHEMDABAD.name(), "6 hours 00 mins", LocalDate.of(2020, 8, 21), LocalTime.of(4, 30),
				MUMBAI.name(), new Fare("INR", 1955D), fI5755);

		Flight f10 = new Flight(KOLKATA.name(), "3 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(4, 45),
				HYDERABAD.name(), new Fare("INR", 2500D), fI5756);

		Flight f11 = new Flight(DELHI.name(), "1 hours 00 mins", LocalDate.of(2020, 8, 21), LocalTime.of(7, 45),
				KOLKATA.name(), new Fare("INR", 4943D), fI5757);

		Flight f12 = new Flight(MUMBAI.name(), "1 hours 00 mins", LocalDate.of(2020, 8, 21), LocalTime.of(11, 45),
				DELHI.name(), new Fare("INR", 4943D), fSG434);

		Flight f13 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(2, 45),
				DELHI.name(), new Fare("INR", 4500D), fSG435);

		Flight f14 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(10, 15),
				DELHI.name(), new Fare("INR", 6200D), fUK830);

		Flight f15 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(11, 30),
				DELHI.name(), new Fare("INR", 5000D), fUK831);

		Flight f16 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(11, 55),
				DELHI.name(), new Fare("INR", 1200D), fUK832);

		Flight f17 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(11, 55),
				DELHI.name(), new Fare("INR", 1389D), fUK833);

		Flight f18 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(10, 15),
				DELHI.name(), new Fare("INR", 11000D), f2T518);

		Flight f19 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(19, 35),
				DELHI.name(), new Fare("INR", 15000D), f2T519);

		flightService.saveOrUpdateAll(
				Arrays.asList(f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15, f16, f17, f18, f19));
	}

	private void createAirlineRecords() {

		AirlineInfo airlineAirIndia = new AirlineInfo("airindia.png", "Air India");
		AirlineInfo airlineIndigo = new AirlineInfo("indigo.png", "Indigo");
		AirlineInfo airlineAirAsia = new AirlineInfo("airasia.png", "Air Asia");
		AirlineInfo airlineSpiceJet = new AirlineInfo("spicejet.png", "Spice Jet");
		AirlineInfo airlineVistara = new AirlineInfo("vistara.png", "Vistara");
		AirlineInfo airlineTrueJet = new AirlineInfo("truejet.png", "True Jet");
		AirlineInfo airlineGoAir = new AirlineInfo("goair.png", "Go Air");

		airlineService.saveOrUpdateAll(Arrays.asList(airlineAirIndia, airlineIndigo, airlineAirAsia, airlineSpiceJet,
				airlineVistara, airlineTrueJet, airlineGoAir));
	}

	public static void main(String[] args) {
		SpringApplication.run(SringbootassignmentApplication.class, args);

	}

}
