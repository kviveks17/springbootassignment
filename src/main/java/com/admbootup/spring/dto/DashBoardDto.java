package com.admbootup.spring.dto;

import java.time.LocalDate;
import java.time.LocalTime;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@RequiredArgsConstructor
public class DashBoardDto {

	private Integer id;
	private String destination;
	private String duration;
	private LocalDate flightDate;
	private String flightNumber;
	private LocalTime flightTime;
	private String origin;
	private Integer fareId;
	private Integer flightInfoId;
	private Integer invoiceId;
	private Integer airlineId;
	private Integer seatCounts;
	private String type;
	private String logo;
	private String name;
}
