package com.admbootup.spring.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.admbootup.spring.entity.FlightInfo;

/**
 * DAO for handling flight informations
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
public interface FlightInfoRepository extends JpaRepository<FlightInfo, Integer> {

	FlightInfo findByFlightNumber(final String flightNumber);
	
}
