package com.admbootup.spring.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.admbootup.spring.entity.AirlineInfo;

/**
 * DAO for handling airline informations
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
public interface AirlineInfoRepository extends JpaRepository<AirlineInfo, Integer> {

	AirlineInfo findByName(final String name);

}
