package com.admbootup.spring.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.admbootup.spring.entity.Flight;

public interface DashboardRepository extends JpaRepository<Flight, Integer> {
	@Query(value = "select test1.*,airline_info.logo,airline_info.name from airline_info inner join\r\n" + 
			"(select id,destination,duration,flight_date,flight.flight_number,flight_time,origin,fare_id,flight.flight_info_id,inv_id,airline_id,\r\n" + 
			"seat_counts,type from flight inner join (select airline_id,flights_info.flight_info_id,flight_number,seat_counts, type  from flights_info inner join flight_info on \r\n" + 
			"flights_info.flight_info_id= flight_info.flight_info_id) as test\r\n" + 
			"on flight.flight_info_id = test.flight_info_id) as test1\r\n" + 
			"on airline_info.airline_id=test1.airline_id", nativeQuery = true)
	List<String> fetchDashboardInformation();
}
