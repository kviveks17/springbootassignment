package com.admbootup.spring.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.admbootup.spring.entity.Fare;

/**
 * DAO for handling fares
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
public interface FareRepository extends JpaRepository<Fare, Integer> {

}
