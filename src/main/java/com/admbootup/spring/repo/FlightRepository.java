package com.admbootup.spring.repo;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.admbootup.spring.entity.Flight;

/**
 * DAO for handling flight informations
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */

public interface FlightRepository extends JpaRepository<Flight, Integer> {

	@Query(value = "select flight from Flight flight where flightInfo.airlineInfo.id=:airlineId and flightDate=:date")
	List<Flight> findByAirlineIdAndDate(@Param("airlineId") Integer airlineId, @Param("date") LocalDate date);

	List<Flight> findByOriginAndFlightDate(final String origin, final LocalDate flightDate);
	
	List<Flight> findByOrigin(final String origin);
	
	Optional<Flight> findById(final Integer id);

	List<Flight> findByFlightDate(final LocalDate date);
	
	List<Flight> findByFlightNumber(final String flightNumber);
	
	List<Flight> findByDestination(final String destination);
	
	List<Flight> findByOriginAndDestinationAndFlightDate(final String origin, final String destination,
			final LocalDate flightDate);

	List<Flight> findSortedRecordsByOriginAndDestinationAndFlightDate(final String origin, final String destination,
			final LocalDate flightDate, final Sort sort);

	@Query(value = "select flight from Flight flight where origin=:origin and destination=:destination "
			+ "and flightDate=:flightDate and inventory.count >= :vacantSeats order by fare.fare asc")
	List<Flight> findFareSortedRecordsByOriginAndDestinationAndFlightDateAndVacantSeats(final String origin,
			final String destination, final LocalDate flightDate, final Integer vacantSeats);

	Flight findByFlightNumberAndOriginAndDestinationAndFlightDateAndFlightTime(final String flightNumber,
			final String origin, final String destination, final LocalDate flightDate, final LocalTime flightTime);

	List<Flight> findByFlightNumberAndOriginAndDestination(final String flightNumber, final String origin,
			final String destination);

	Flight findByFlightNumberAndFlightDateAndFlightTime(final String flightNumber, final LocalDate flightDate, final LocalTime flightTime);

}
