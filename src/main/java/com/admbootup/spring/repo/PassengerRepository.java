package com.admbootup.spring.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.admbootup.spring.entity.Passenger;

public interface PassengerRepository extends JpaRepository<Passenger, Integer> {

	Passenger findByEmailId(final String emailId);

}
