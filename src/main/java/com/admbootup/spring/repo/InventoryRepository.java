package com.admbootup.spring.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.admbootup.spring.entity.Inventory;

/**
 * DAO for handling inventory informations
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
public interface InventoryRepository extends JpaRepository<Inventory, Integer> {

}
