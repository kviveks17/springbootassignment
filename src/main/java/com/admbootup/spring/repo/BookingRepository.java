package com.admbootup.spring.repo;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.data.jpa.repository.JpaRepository;

import com.admbootup.spring.entity.BookingRecord;

public interface BookingRepository extends JpaRepository<BookingRecord, Integer> {
	
	BookingRecord findByFlightNumberAndOriginAndDestinationAndTripDateAndTripTime(final String flightNumber, final String origin,
			final String destination, final LocalDate tripDate, final LocalTime tripTime);

}
