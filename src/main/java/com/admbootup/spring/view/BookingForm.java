package com.admbootup.spring.view;

import java.util.ArrayList;
import java.util.List;

import com.admbootup.spring.entity.BookingRecord;
import com.admbootup.spring.entity.Flight;
import com.admbootup.spring.entity.Passenger;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingForm {

	private List<Passenger> passengers = new ArrayList<>();
	private Passenger passenger;
	private Flight flight;
	private String flightId;
	private BookingRecord bookingRecord;
	private boolean bookingDone;
	

}
