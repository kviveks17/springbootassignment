package com.admbootup.spring.view;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.admbootup.spring.entity.BookingRecord;
import com.admbootup.spring.entity.Flight;
import com.admbootup.spring.entity.Passenger;
import com.admbootup.spring.service.BookingService;
import com.admbootup.spring.service.FlightService;
import com.admbootup.spring.service.PassengerService;
import com.admbootup.spring.util.CITIES;

@Controller
public class FlightsController {

	@Autowired
	private FlightService flightService;

	@Autowired
	private BookingService bookingService;

	@Autowired
	private PassengerService passengerService;

	@RequestMapping(path = { "/ui/flights/search" }, method = RequestMethod.GET)
	public String loadSearchForm(ModelMap map) {
		map.addAttribute("searchForm", new SearchForm());
		return "flights_search";
	}

	@RequestMapping(path = { "/ui/flights/search" }, method = RequestMethod.POST)
	public String LoadSearchFormPost(ModelMap map) {
		return loadSearchForm(map);
	}

	@RequestMapping(path = { "/ui/flights/search-submit" }, method = RequestMethod.POST)
	public String listFlights(@ModelAttribute SearchForm searchForm, ModelMap map) {

		List<Flight> flights = flightService.fetchFlightsFromOriginToDestinationOnDate(searchForm.getOrigin(),
				searchForm.getDestination(), searchForm.getDate());
		searchForm.setFlights(flights);
		searchForm.setShowResults(!CollectionUtils.isEmpty(flights));
		map.addAttribute("searchForm", searchForm);

		return "flights_search";
	}

	@RequestMapping(path = { "/ui/flights/booking" }, method = RequestMethod.POST)
	public String bookFlight(@RequestParam(name = "flightId", required = true) String flightId, ModelMap map) {
		Flight flight = null;
		if (!StringUtils.isEmpty(flightId)) {
			flight = flightService.findById(Integer.parseInt(flightId));
		}

		BookingForm booking = new BookingForm();
		booking.setFlight(flight);
		booking.setFlightId(flightId);
		booking.setPassenger(new Passenger());
		map.addAttribute("bookingForm", booking);

		return "flights_booking";
	}

	@RequestMapping(path = { "/ui/flights/booking/submit" }, method = RequestMethod.POST)
	public String bookFlight(@RequestParam(name = "addPassenger", required = false) String addPassenger,
			@RequestParam(name = "bookFlight", required = false) String bookFlight,
			@ModelAttribute("bookingForm") BookingForm form, ModelMap map) {
		if (!StringUtils.isEmpty(addPassenger)) {
			if (!StringUtils.isEmpty(form.getFlightId())) {
				Flight flight = flightService.findById(Integer.parseInt(form.getFlightId()));
				form.setFlight(flight);
			}

			form.setPassenger(new Passenger());
			map.addAttribute("bookingForm", form);

		}
		if (!StringUtils.isEmpty(bookFlight)) {
			if (!StringUtils.isEmpty(form.getFlightId())) {
				Flight flight = flightService.findById(Integer.parseInt(form.getFlightId()));
				form.setFlight(flight);
			}
			form.getPassengers().add(form.getPassenger());
			BookingRecord bookingRecord = bookingService.bookFlight(form.getFlight(), form.getPassengers());
			
			if (!CollectionUtils.isEmpty(bookingRecord.getPassengers()) && bookingRecord.getPassengers().toArray()[0] !=null) {
				form.setPassengers(new ArrayList<>());
				bookingRecord.getPassengers().forEach(form.getPassengers()::add);
			}
			form.setBookingDone(true);
			form.setPassenger(null);

			if (bookingRecord != null) {
				form.setBookingRecord(bookingRecord);
			}
			map.addAttribute("bookingForm", form);
		}
		return "flights_booking";

	}

	private List<Passenger> getPassengers(final List<Passenger> passengers) {
		for (Passenger pass : passengers) {
			if (StringUtils.isEmpty(pass.getPassword())) {
				pass.setPassword((pass.getFirstName() + pass.getLastName()).toLowerCase());
			}
		}

		List<Passenger> passengerSaved = passengerService.saveOrUpdateAll(passengers);
		return passengerSaved;
	}

	@ModelAttribute(name = "cities", value = "cities")
	private List<String> getCities() {
		List<String> cities = new ArrayList<>();
		for (CITIES city : CITIES.values()) {
			cities.add(city.name());
		}
		return cities;
	}
	

	
	@ModelAttribute(name = "userName")
    public String currentUserName(Principal principal) {
		 if(principal==null)
			 principal = (Principal) SecurityContextHolder. getContext(). getAuthentication(). getPrincipal();
        return principal.getName();
    }

}
