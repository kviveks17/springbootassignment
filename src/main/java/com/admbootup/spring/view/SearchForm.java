package com.admbootup.spring.view;

import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.CollectionUtils;

import com.admbootup.spring.entity.Flight;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SearchForm {

	private String origin;
	private String destination;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate date;

	private List<Flight> flights;

	private boolean showResults;
		
	@PostConstruct
	public void init() {
		if (!CollectionUtils.isEmpty(flights))
			showResults = true;
	}

}
