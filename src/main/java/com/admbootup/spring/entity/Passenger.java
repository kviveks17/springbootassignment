package com.admbootup.spring.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Passenger Information
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "emailId" }, name = "UNIQUE_PASSENGER_EMAIL"))
@SuppressWarnings("unused")
public class Passenger {

	@Id
	@GeneratedValue
	@Column(name = "passenger_id")
	private Integer id;

	private String firstName;

	private String lastName;

	private String initial;

	private String emailId;

	private String mobileNumber;

	private String gender;
	@NotNull
	private String password;

	public Passenger() {
		super();
	}

	public Passenger(final String firstName, final String lastName, final String initial, final String emailId,
			final String mobileNumber, final String gender, final String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.initial = initial;
		this.emailId = emailId;
		this.mobileNumber = mobileNumber;
		this.gender = gender;
		this.password = password;
	}

}