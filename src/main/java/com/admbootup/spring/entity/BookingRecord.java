package com.admbootup.spring.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Booking records for the flight
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@SuppressWarnings("unused")
public class BookingRecord {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "booking_id")
	private Integer id;

	private LocalDate tripDate;

	private LocalTime tripTime;

	private String origin;

	private String destination;

	private String tripDuration;

	private Double fare;

	private String status;

	private LocalDateTime bookingDate;

	private String flightNumber;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "booking_details", joinColumns = @JoinColumn(name = "booking_id"), inverseJoinColumns = @JoinColumn(name = "passenger_id"))
	private Set<Passenger> passengers= new HashSet<>();

	public BookingRecord() {

	}

}
