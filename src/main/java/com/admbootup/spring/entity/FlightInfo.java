package com.admbootup.spring.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.admbootup.spring.util.FlightType;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Flight information entity
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode

@Entity(name = "flight_info")
@SuppressWarnings("unused")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "flightNumber" }, name = "UniqueFlightInformationNumber"))
public class FlightInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "flight_info_id")
	private Integer infoId;

	private String flightNumber;

	private String type;

	private Integer seatCounts;

	@ManyToOne
	@JoinTable(name = "flights_info", joinColumns = @JoinColumn(name = "flight_info_id"), inverseJoinColumns = @JoinColumn(name = "airline_id"))
	private AirlineInfo airlineInfo;

	@JsonManagedReference
	@OneToMany
	@JoinColumn(name = "flight_info_id")
	private List<Flight> flights;

	public FlightInfo(final String flightNumber, final String type, final Integer seatCounts) {
		super();
		this.flightNumber = flightNumber;
		this.type = type;
		this.seatCounts = seatCounts;
	}
	
	public FlightInfo(final String flightNumber, final FlightType flightType, final AirlineInfo airlineInfo) {
		super();
		this.flightNumber = flightNumber;
		this.type = flightType.name();
		this.seatCounts = flightType.getSeatCounts();
		this.airlineInfo = airlineInfo;
	}

	public FlightInfo() {
		super();
	}

}
