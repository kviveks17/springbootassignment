package com.admbootup.spring.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Inventory for the flight Application
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@SuppressWarnings("unused")
public class Inventory {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "inv_id")
	private Integer id;

	private Integer count;

	public Inventory(Integer count) {
		super();
		this.count = count;
	}

	public Inventory() {
	}

}
