/**
 * 
 */
package com.admbootup.spring.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Airline information entity
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
@ToString
@Getter
@EqualsAndHashCode
@AllArgsConstructor
@Setter
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "name" }, name = "uniqueAirlineName"))
@SuppressWarnings("unused")
public class AirlineInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "airline_id")
	private Integer id;

	private String logo;

	private String name;

	@JsonIgnore
	@Transient
	@OneToMany(mappedBy = "airlineInfo")
	private List<FlightInfo> flightInfos;

	public AirlineInfo() {

	}

	public AirlineInfo(final String logo, final String name) {
		super();
		this.logo = logo;
		this.name = name;
	}

	/**
	 * list contains the flights associated with airlines
	 */
	@OneToMany
	@JoinTable(name = "flights_info", joinColumns = @JoinColumn(name = "airline_id"), inverseJoinColumns = @JoinColumn(name = "flight_info_id"))
	private List<FlightInfo> airlineFlights;

}
