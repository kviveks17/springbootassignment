package com.admbootup.spring.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Fare for the flight
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@SuppressWarnings("unused")
public class Fare {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer fareId;

	private String currency;

	private Double fare;

	public Fare(String currency, Double fare) {
		super();
		this.currency = currency;
		this.fare = fare;
	}

	public Fare() {
		super();
	}

	
}
