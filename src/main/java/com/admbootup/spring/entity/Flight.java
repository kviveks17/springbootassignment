package com.admbootup.spring.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Flight entity to hold data for flights
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
@Entity
@Getter
@Data
@Setter
@EqualsAndHashCode
@ToString(exclude = "flightInfo")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "flightNumber", "flightDate",
		"flightTime" }, name = "UNIQUE_FlightNumber_FlightDate_FlightTime"))
@SuppressWarnings("unused")
public class Flight implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String destination;

	private String duration;

	private LocalDate flightDate;

	private LocalTime flightTime;

	private String flightNumber;

	private String origin;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fare_id")
	private Fare fare;

	@JsonBackReference
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "flight_info_id")
	private FlightInfo flightInfo;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "inv_id")
	private Inventory inventory;

	public Flight() {

	}

	public Flight(final String destination, final String duration, final LocalDate flightDate,
			final LocalTime flightTime, final String origin, final Fare fare, final FlightInfo flightInfo) {
		super();
		this.destination = destination;
		this.duration = duration;
		this.flightDate = flightDate;
		this.flightTime = flightTime;
		this.flightInfo = flightInfo;
		this.flightNumber = flightInfo.getFlightNumber();
		this.origin = origin;
		this.fare = fare;
		this.inventory = new Inventory(flightInfo.getSeatCounts());
	}

}
