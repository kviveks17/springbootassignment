package com.admbootup.spring.rest.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.admbootup.spring.entity.BookingRecord;
import com.admbootup.spring.entity.Flight;
import com.admbootup.spring.repo.BookingRepository;

@RestController
@RequestMapping("/flight-booking")
public class BookingController {
	@Autowired
	private BookingRepository bookingRepository;

	@PostMapping(path = "/flight/book", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BookingRecord> bookFlight(@RequestBody BookingRecord bookingRecord) {
		return ResponseEntity.ok(bookingRepository.saveAndFlush(bookingRecord));
	}

	@GetMapping(path = "/bookings", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<BookingRecord>> getFlights() {
		List<BookingRecord> bookingRecords = bookingRepository.findAll();
		return ResponseEntity.ok(bookingRecords);
	}

	@GetMapping(path = "/booking/{id}", produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<BookingRecord> getFlights(@PathVariable Integer id) {
		return ResponseEntity.ok(
				Optional.ofNullable(bookingRepository.findById(id)).get().orElseThrow(IllegalArgumentException::new));
	}

	@PutMapping("/booking/updateBooking")
	public ResponseEntity<BookingRecord> updateFlight(@Valid @RequestBody BookingRecord bookingRecord) {
		Optional.ofNullable(bookingRepository.findById(bookingRecord.getId()))
				.map(existingBooking -> bookingRepository.save(bookingRecord))
				.orElseThrow(IllegalArgumentException::new);
		return ResponseEntity.ok(bookingRecord);
	}
	
	@DeleteMapping("/booking/delete-booking/{id}")
	public ResponseEntity<Integer> deleteUser(@PathVariable(value = "id") Integer bookingId) throws Exception {
		Optional<BookingRecord> existingBooking =bookingRepository.findById(bookingId);
		if(existingBooking.isPresent()) {
			bookingRepository.delete(existingBooking.get());
		}else {
			throw new IllegalArgumentException("Booking does not exist");
		}
		
		return new ResponseEntity<>(bookingId, HttpStatus.OK);
	}


}
