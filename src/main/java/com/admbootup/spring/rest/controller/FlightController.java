package com.admbootup.spring.rest.controller;

import static com.admbootup.spring.util.CITIES.AHEMDABAD;
import static com.admbootup.spring.util.CITIES.BENGALURU;
import static com.admbootup.spring.util.CITIES.CHENNAI;
import static com.admbootup.spring.util.CITIES.DELHI;
import static com.admbootup.spring.util.CITIES.HYDERABAD;
import static com.admbootup.spring.util.CITIES.KOLKATA;
import static com.admbootup.spring.util.CITIES.MANGALORE;
import static com.admbootup.spring.util.CITIES.MUMBAI;
import static com.admbootup.spring.util.CITIES.PORTBLAIR;
import static com.admbootup.spring.util.CITIES.PUNE;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.admbootup.spring.entity.Fare;
import com.admbootup.spring.entity.Flight;
import com.admbootup.spring.entity.FlightInfo;
import com.admbootup.spring.repo.FlightRepository;
import com.admbootup.spring.service.FlightInfoService;
import com.admbootup.spring.service.FlightService;

@RestController
@RequestMapping("/rest")
public class FlightController {

	@Autowired
	private FlightRepository flightRepository;

	@Autowired
	private FlightService flightService;

	@Autowired
	private FlightInfoService flightInfoService;

	@PostMapping(path = "/flights/add", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Flight> addFlights(@RequestBody Flight flight) {

		return ResponseEntity.ok(flightRepository.saveAndFlush(flight));
	}

	@GetMapping(path = "/flights", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Flight> getFlights() {
		List<Flight> flights = flightRepository.findAll();
		return flights;
	}

	@GetMapping(path = "/flight/city-name/{city}")
	public List<Flight> getFlightsBySource(@PathVariable String city) {
		List<Flight> flightsBySource = flightRepository.findByOrigin(city);
		return flightsBySource;
	}

	@GetMapping(path = "/flight/date/{date}")
	public List<Flight> getFlightsByDate(@PathVariable String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		List<Flight> flightsByDate = flightRepository.findByFlightDate(LocalDate.parse(date, formatter));
		return flightsByDate;
	}

	@GetMapping(path = "/flight/flight-number/{flightNumber}")
	public List<Flight> getFlightsByFlightNumber(@PathVariable String flightNumber) {
		List<Flight> flightsByFlightNumber = flightRepository.findByFlightNumber(flightNumber);
		return flightsByFlightNumber;
	}

	@GetMapping(path = "/flight/destination/{destination}")
	public List<Flight> getFlightsByDestination(@PathVariable String destination) {
		List<Flight> flightsByDestination = flightRepository.findByDestination(destination);
		return flightsByDestination;
	}

	@PutMapping("/flight/updateFlight")
	public ResponseEntity<Flight> updateFlight(@Valid @RequestBody Flight flightDetails) {
		Optional
		.ofNullable(flightRepository.findById(flightDetails.getId()))
		.map(existingFlight -> 
			flightRepository.save(flightDetails))
		.orElseThrow(IllegalArgumentException::new);
		return ResponseEntity.ok(flightDetails);
	}

	@DeleteMapping("/flight/delete-flight/{id}")
	public ResponseEntity<Integer> deleteUser(@PathVariable(value = "id") Integer flightId) throws Exception {
		flightRepository.findById(flightId).ifPresent(existingFlight -> flightRepository.delete(existingFlight));
		return new ResponseEntity<>(flightId, HttpStatus.OK);
	}

	private void createFlightRecords() {

		FlightInfo ai840 = flightInfoService.getByName("AI-840");
		FlightInfo ai850 = flightInfoService.getByName("AI-850");
		FlightInfo ai860 = flightInfoService.getByName("AI-860");
		FlightInfo ai870 = flightInfoService.getByName("AI-870");
		FlightInfo f6e6684 = flightInfoService.getByName("6E-6684");
		FlightInfo f6e6685 = flightInfoService.getByName("6E-6685");
		FlightInfo f6e6686 = flightInfoService.getByName("6E-6686");
		FlightInfo f6e6687 = flightInfoService.getByName("6E-6687");
		FlightInfo fI5755 = flightInfoService.getByName("I5-755");
		FlightInfo fI5756 = flightInfoService.getByName("I5-756");
		FlightInfo fI5757 = flightInfoService.getByName("I5-757");
		FlightInfo fSG434 = flightInfoService.getByName("SG-434");
		FlightInfo fSG435 = flightInfoService.getByName("SG-435");
		FlightInfo fUK830 = flightInfoService.getByName("UK-830");
		FlightInfo fUK831 = flightInfoService.getByName("UK-831");
		FlightInfo fUK832 = flightInfoService.getByName("UK-832");
		FlightInfo fUK833 = flightInfoService.getByName("UK-833");
		FlightInfo f2T518 = flightInfoService.getByName("2T-518");
		FlightInfo f2T519 = flightInfoService.getByName("2T-519");

		Flight f1 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(2, 12),
				DELHI.name(), new Fare("INR", 4500D), ai840);

		Flight f2 = new Flight(HYDERABAD.name(), "2 hours 45 mins", LocalDate.of(2020, 8, 21), LocalTime.of(1, 15),
				DELHI.name(), new Fare("INR", 3500D), ai850);

		Flight f3 = new Flight(MUMBAI.name(), "2 hours 50 mins", LocalDate.of(2020, 8, 21), LocalTime.of(2, 30),
				CHENNAI.name(), new Fare("INR", 5000D), ai860);

		Flight f4 = new Flight(HYDERABAD.name(), "1 hours 45 mins", LocalDate.of(2020, 8, 21), LocalTime.of(2, 45),
				PUNE.name(), new Fare("INR", 2546D), ai870);

		Flight f5 = new Flight(PORTBLAIR.name(), "3 hours 00 mins", LocalDate.of(2020, 8, 21), LocalTime.of(3, 00),
				CHENNAI.name(), new Fare("INR", 7500D), f6e6684);

		Flight f6 = new Flight(DELHI.name(), "3 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(3, 15),
				BENGALURU.name(), new Fare("INR", 10000D), f6e6685);

		Flight f7 = new Flight(PUNE.name(), "4 hours 07 mins", LocalDate.of(2020, 8, 21), LocalTime.of(3, 15),
				DELHI.name(), new Fare("INR", 7438D), f6e6686);

		Flight f8 = new Flight(MANGALORE.name(), "3 hours 30 mins", LocalDate.of(2020, 8, 21), LocalTime.of(3, 15),
				HYDERABAD.name(), new Fare("INR", 8743D), f6e6687);

		Flight f9 = new Flight(AHEMDABAD.name(), "6 hours 00 mins", LocalDate.of(2020, 8, 21), LocalTime.of(4, 30),
				MUMBAI.name(), new Fare("INR", 1955D), fI5755);

		Flight f10 = new Flight(KOLKATA.name(), "3 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(4, 45),
				HYDERABAD.name(), new Fare("INR", 2500D), fI5756);

		Flight f11 = new Flight(DELHI.name(), "1 hours 00 mins", LocalDate.of(2020, 8, 21), LocalTime.of(7, 45),
				KOLKATA.name(), new Fare("INR", 4943D), fI5757);

		Flight f12 = new Flight(MUMBAI.name(), "1 hours 00 mins", LocalDate.of(2020, 8, 21), LocalTime.of(11, 45),
				DELHI.name(), new Fare("INR", 4943D), fSG434);

		Flight f13 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(2, 45),
				DELHI.name(), new Fare("INR", 4500D), fSG435);

		Flight f14 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(10, 15),
				DELHI.name(), new Fare("INR", 6200D), fUK830);

		Flight f15 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(11, 30),
				DELHI.name(), new Fare("INR", 5000D), fUK831);

		Flight f16 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(11, 55),
				DELHI.name(), new Fare("INR", 1200D), fUK832);

		Flight f17 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(11, 55),
				DELHI.name(), new Fare("INR", 1389D), fUK833);

		Flight f18 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(10, 15),
				DELHI.name(), new Fare("INR", 11000D), f2T518);

		Flight f19 = new Flight(CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 21), LocalTime.of(19, 35),
				DELHI.name(), new Fare("INR", 15000D), f2T519);

		flightService.saveOrUpdateAll(
				Arrays.asList(f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15, f16, f17, f18, f19));
	}

}
