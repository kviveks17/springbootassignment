package com.admbootup.spring.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.admbootup.spring.dto.DashBoardDto;
import com.admbootup.spring.repo.DashboardRepository;
import com.admbootup.spring.util.DashBoardUtil;

@RestController
public class DashboardController {
	@Autowired
	DashboardRepository dashboardRepository;
	@GetMapping("/dashboard")
	public List<DashBoardDto> getDashboardInformation() {
		List<String> dashBoardData = dashboardRepository.fetchDashboardInformation();
		List<DashBoardDto> dtoDBList = DashBoardUtil.convertDataToDto(dashBoardData);
		return dtoDBList;
	}
}
