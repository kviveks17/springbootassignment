package com.admbootup.spring.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.admbootup.spring.entity.AirlineInfo;
import com.admbootup.spring.service.AirlineInfoService;

@RestController
public class AirlineController {
	@Autowired
	private AirlineInfoService airlineInfoService;

	@GetMapping(path = "/rest/airlines", produces=MediaType.APPLICATION_JSON_VALUE)
	private List<AirlineInfo> getAllAirlines() {
		return airlineInfoService.getAllAirlineInformation();
	}
}
