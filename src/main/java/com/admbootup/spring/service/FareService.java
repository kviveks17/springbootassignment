package com.admbootup.spring.service;

import java.util.List;

import com.admbootup.spring.entity.Fare;

/**
 * Service implementation for handling fares
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
public interface FareService {

	/**
	 * Create a fare
	 * 
	 * @param fare - fare object
	 */
	Fare saveOrUpdate(final Fare fare);

	/**
	 * Deletes a fare object
	 * 
	 * @param fareId - fare id for the fare object
	 */
	void delete(final Integer fareId);

	/**
	 * Gets all the fares present
	 * 
	 * @return List of fares
	 */
	List<Fare> getAllFares();

	/**
	 * Returns fare by id
	 * 
	 * @param id - id for the fare
	 * @return {@link Fare}
	 */
	Fare getFareById(final Integer id);

}
