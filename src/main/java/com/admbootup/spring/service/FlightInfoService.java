package com.admbootup.spring.service;

import java.util.List;

import com.admbootup.spring.entity.FlightInfo;

/**
 * Service implementation for handling flight information
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
public interface FlightInfoService {

	/**
	 * Gets all flight informations
	 * 
	 * @return list of flight information
	 */
	List<FlightInfo> getAllFlightInfos();
	
	FlightInfo getByName(final String Name);

	/**
	 * Returns the flight information by id
	 * 
	 * @param id - id for which flight information is required
	 * @return flight information for the specified id
	 */
	FlightInfo getFlightInfo(final Integer id);

	/**
	 * Saves or updates the flight information
	 * 
	 * @param flightInfo to be persisted
	 * @return attached flight information
	 */
	FlightInfo updateOrSave(final FlightInfo flightInfo);

	/**
	 * Saves or updates the flight information
	 * 
	 * @param flightInfo to be persisted
	 * @return attached flight information
	 */
	Iterable<FlightInfo> updateOrSaveAll(final Iterable<FlightInfo> flightInfo);

	/**
	 * Delete flight information for the id
	 * 
	 * @param id - identifier for the flight information to be removed
	 * 
	 */
	void delete(final Integer id);

}
