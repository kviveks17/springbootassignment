package com.admbootup.spring.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.data.domain.Sort;

import com.admbootup.spring.entity.Flight;

/**
 * Service implementation for handling flight information
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
public interface FlightService {

	/**
	 * Lists all the flights
	 * 
	 * @return list of all the flights
	 */
	List<Flight> findAllFlights();

	/**
	 * Find the flight details with the specified ID
	 * 
	 * @param id - id for the flight
	 * @return flight {@link Flight}
	 */
	Flight findById(final Integer id);

	/**
	 * Saves or updates the flight information
	 * 
	 * @param flight - flight information to be saved
	 * @return the attached flight information
	 */
	Flight saveOrUpdate(final Flight flight);

	/**
	 * Saves or updates all the flight information
	 * 
	 * @param flights flights to be persisted
	 * @return all the attached flight information
	 */
	Iterable<Flight> saveOrUpdateAll(final Iterable<Flight> flights);

	/**
	 * Deletes the flight information
	 * 
	 * @param id - specified id for the flight for deletion
	 */
	void delete(final Integer id);

	/**
	 * returns the list of a particular airlines that operated on a particular date
	 * on the basis of specific airline id and specific date
	 * 
	 * @param id   - id for which airline information to be fetched
	 * 
	 * @param date - date for which airline information to be fetched
	 */
	List<Flight> fetchFlightsForAirlineAndDate(final Integer airlineId, final LocalDate date);

	/**
	 * returns the list of airlines that operated from a particular origin on a
	 * specific date
	 * 
	 * @param origin - origin from which airline information to be fetched
	 * 
	 * @param date   - date for which airline information to be fetched
	 */
	List<Flight> fetchFlightsFromSpecificOriginAndDate(final String origin, final LocalDate date);

	/**
	 * returns the list of airlines that operated from a particular origin to a
	 * particular destination on a specific date
	 * 
	 * @param origin      - origin from which airline information to be fetched
	 * 
	 * @param destination - destination to which airline information is to be
	 *                    fetched
	 * @param date        - date for which flight information is required
	 * 
	 * @return list of flights
	 */
	List<Flight> fetchFlightsFromOriginToDestinationOnDate(final String origin, final String destination,
			final LocalDate date);

	/**
	 * returns the list of airlines that operated from a particular origin to a
	 * particular destination on a specific date
	 * 
	 * @param origin      - origin from which airline information to be fetched
	 * 
	 * @param destination - destination to which airline information is to be
	 *                    fetched
	 * @param date        - date for which flight information is required
	 * 
	 * @param sort        - sorting field for the records
	 * 
	 * @return list of flights
	 */
	List<Flight> fetchSortedFlightsFromOriginToDestinationOnDate(final String origin, final String destination,
			final LocalDate date, final Sort sort);

	/**
	 * returns the list of airlines that operated from a particular origin to a
	 * particular destination on a specific date
	 * 
	 * @param origin              - origin from which airline information to be
	 *                            fetched
	 * 
	 * @param destination         - destination to which airline information is to
	 *                            be fetched
	 * @param date                - date for which flight information is required
	 * 
	 * @param vacantSeatsRequired - number of vacant seats required
	 * 
	 * 
	 * @return list of flights
	 */
	List<Flight> fetchSortedFlightsFromOriginToDestinationOnDateAndVacantPassenger(final String origin,
			final String destination, final LocalDate date, final Integer vacantSeatsRequired);

	/**
	 * returns the list of airlines on the basis of flight number, origin and
	 * destination
	 * 
	 * @param flightNumber - flight number for which information is to be fetched
	 * 
	 * @param origin       - origin from which airline information is to be fetched
	 * 
	 * @param destination  - destination to which airline information is to be
	 *                     fetched
	 */
	List<Flight> fetchFlightsFromSpecificFlightNumberOriginAndDestination(final String flightNumber,
			final String origin, final String destination);

	/**
	 * returns the list of airlines on the basis of flight number, flight date and
	 * flight time
	 * 
	 * @param flightNumber - flight number for which information is to be fetched
	 * 
	 * @param flightDate   - date on which airline information is to be fetched
	 * 
	 * @Param flightTime - time on which flight is scheduled
	 * 
	 * @return flight information
	 */
	Flight fetchFlightsFromSpecificFlightNumberAndFlightDateAndTime(final String flightNumber,
			final LocalDate flightDate, final LocalTime flightTime);
}
