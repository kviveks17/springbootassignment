package com.admbootup.spring.service;

import java.util.List;

import com.admbootup.spring.entity.Inventory;

/**
 * Service implementation for handling inventory information
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
public interface InventoryService {

	List<Inventory> getAllInventories();

	Inventory getById(final Integer id);

	Inventory saveOrUpdate(final Inventory inventory);

	void delete(final Integer id);

}
