package com.admbootup.spring.service;

import java.util.List;

import com.admbootup.spring.entity.AirlineInfo;

public interface AirlineInfoService {

	/**
	 * Creates the specified airline information record
	 * 
	 * @param airlineInfo -airline information detail
	 */
	AirlineInfo saveOrUpdate(final AirlineInfo airlineInfo);

	/**
	 * Creates the specified airline information records
	 * 
	 * @param airlineInfos -airlines information
	 */
	List<AirlineInfo> saveOrUpdateAll(Iterable<AirlineInfo> airlineInfos);

	/**
	 * Deletes the airline info with the specified id
	 * 
	 * @param id - id for which the airline info is to be deleted
	 */
	void delete(final Integer id);

	/**
	 * Searches the airline information by the specified id
	 * 
	 * @param id - id for which airline information to be fetched
	 */
	AirlineInfo getAirlineInfoById(final Integer id);

	/**
	 * Searches the airline information by the specified name
	 * 
	 * @param name - name for which airline information to be fetched
	 */
	AirlineInfo getAirlineInfoByName(final String name);

	/**
	 * returns the list of all the airlines information
	 * 
	 * @return list of airline informations
	 */
	List<AirlineInfo> getAllAirlineInformation();

}
