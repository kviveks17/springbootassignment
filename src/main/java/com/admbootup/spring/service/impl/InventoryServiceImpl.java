package com.admbootup.spring.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.admbootup.spring.entity.Inventory;
import com.admbootup.spring.repo.InventoryRepository;
import com.admbootup.spring.service.InventoryService;

@Service
public class InventoryServiceImpl implements InventoryService {

	@Autowired
	private InventoryRepository inventoryRepository;

	@Override
	public List<Inventory> getAllInventories() {
		List<Inventory> inventories = new ArrayList<>();
		inventoryRepository.findAll().forEach(inventories::add);
		return inventories;
	}

	@Override
	public Inventory getById(final Integer id) {
		return inventoryRepository.findById(id).get();
	}

	@Override
	public Inventory saveOrUpdate(final Inventory inventory) {
		return inventoryRepository.save(inventory);
	}

	@Override
	public void delete(Integer id) {
		inventoryRepository.deleteById(id);
	}

}
