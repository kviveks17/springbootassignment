package com.admbootup.spring.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.admbootup.spring.entity.Fare;
import com.admbootup.spring.repo.FareRepository;
import com.admbootup.spring.service.FareService;

@Service
public class FareServiceImpl implements FareService {

	@Autowired
	private FareRepository fareRepository;

	@Override
	public Fare saveOrUpdate(final Fare fare) {
		return fareRepository.save(fare);
	}

	@Override
	public void delete(Integer fareId) {
		fareRepository.deleteById(fareId);
	}

	@Override
	public List<Fare> getAllFares() {
		List<Fare> fares = new ArrayList<>();
		fareRepository.findAll().forEach(fares::add);
		return fares;
	}

	@Override
	public Fare getFareById(final Integer id) {
		return fareRepository.findById(id).get();
	}

}
