package com.admbootup.spring.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.admbootup.spring.entity.AirlineInfo;
import com.admbootup.spring.repo.AirlineInfoRepository;
import com.admbootup.spring.service.AirlineInfoService;

@Service
public class AirlineInfoServiceImpl implements AirlineInfoService {

	@Autowired
	private AirlineInfoRepository airlineInfoRepository;

	@Override
	public AirlineInfo saveOrUpdate(final AirlineInfo airlineInfo) {
		return saveOrUpdateAll(Arrays.asList(airlineInfo)).get(0);
	}

	@Override
	public List<AirlineInfo> saveOrUpdateAll(final Iterable<AirlineInfo> airlineInfos) {
		return airlineInfoRepository.saveAll(retrieveAirlineInfoByName(airlineInfos));
	}

	private List<AirlineInfo> retrieveAirlineInfoByName(final Iterable<AirlineInfo> airlineInfos) {
		if (airlineInfos == null)
			return null;
		List<AirlineInfo> airlines = new ArrayList<>();
		for (AirlineInfo airline : airlineInfos) {
			AirlineInfo dbAirline = airlineInfoRepository.findByName(airline.getName());
			if (dbAirline != null) {
				airlines.add(dbAirline);
			} else {
				airlines.add(airline);
			}
		}
		return airlines;
	}

	@Override
	public void delete(Integer id) {
		airlineInfoRepository.deleteById(id);
	}

	@Override
	public AirlineInfo getAirlineInfoById(Integer id) {
		return airlineInfoRepository.findById(id).get();
	}

	@Override
	public AirlineInfo getAirlineInfoByName(String name) {
		return airlineInfoRepository.findByName(name);
	}

	@Override
	public List<AirlineInfo> getAllAirlineInformation() {
		List<AirlineInfo> airlines = new ArrayList<>();
		airlineInfoRepository.findAll().forEach(airlines::add);
		return airlines;
	}

}
