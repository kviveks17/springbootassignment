package com.admbootup.spring.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.admbootup.spring.config.UserPrincipal;
import com.admbootup.spring.entity.Passenger;
import com.admbootup.spring.repo.PassengerRepository;
import com.admbootup.spring.service.PassengerService;

@Service
public class PassengerServiceImpl implements PassengerService, UserDetailsService {

	@Autowired
	private PassengerRepository passengerRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Passenger passenger = passengerRepository.findByEmailId(username);
		if (passenger == null)
			throw new UsernameNotFoundException("User 404");
		return new UserPrincipal(passenger);
	}

	@Override
	public List<Passenger> findAll() {
		return passengerRepository.findAll();
	}

	private List<Passenger> retrieveExisitngAttachedPassengers(List<Passenger> passengers) {
		List<Passenger> updatedPassengers = new ArrayList<>();
		for (Passenger passenger : passengers) {
			Passenger dbRecord = passengerRepository.findByEmailId(passenger.getEmailId());
			if (dbRecord != null) {
				if (Objects.isNull(dbRecord.getPassword())) {
					dbRecord.setPassword(passwordEncoder.encode(passenger.getPassword()));
					updatedPassengers.add(dbRecord);
				}
			} else {
				updatedPassengers.add(passenger);
			}
		}
		return updatedPassengers;
	}

	@Override
	public List<Passenger> saveOrUpdateAll(List<Passenger> passengers) {
		passengers.forEach(passenger -> {
			if (Objects.nonNull(passenger.getPassword()))
				passenger.setPassword(passwordEncoder.encode(passenger.getPassword()));
		});
		return passengerRepository.saveAll(retrieveExisitngAttachedPassengers(passengers));
	}

	@Override
	public Passenger saveOrUpdate(Passenger passenger) {
		List<Passenger> passList = retrieveExisitngAttachedPassengers(Arrays.asList(passenger));
		passList = passengerRepository.saveAll(passList);

		return CollectionUtils.isEmpty(passList) ? null : passList.get(0);
	}

	@Override
	public void deleteById(Integer passengerId) {
		passengerRepository.deleteById(passengerId);
	}

	@Override
	public Passenger findById(Integer passengerId) {
		return passengerRepository.findById(passengerId).get();
	}

	@Override
	public Passenger findByEmailId(String emailId) {
		return passengerRepository.findByEmailId(emailId);
	}

}
