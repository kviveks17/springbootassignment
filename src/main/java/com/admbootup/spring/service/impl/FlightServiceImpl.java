package com.admbootup.spring.service.impl;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.admbootup.spring.entity.Flight;
import com.admbootup.spring.repo.FlightRepository;
import com.admbootup.spring.service.FlightService;

@Service
public class FlightServiceImpl implements FlightService {

	@Autowired
	private FlightRepository flightRepository;

	@Override
	public List<Flight> findAllFlights() {
		List<Flight> flights = new ArrayList<>();
		flightRepository.findAll().forEach(flights::add);
		return flights;
	}

	@Override
	public List<Flight> saveOrUpdateAll(Iterable<Flight> flights) {

		return flightRepository.saveAll(retrieveAttachedFlights(flights));
	}

	public List<Flight> retrieveAttachedFlights(Iterable<Flight> flights) {
		List<Flight> updatedList = new ArrayList<>();
		for (Flight flight : flights) {
			Flight dbRec = flightRepository.findByFlightNumberAndFlightDateAndFlightTime(flight.getFlightNumber(),
					flight.getFlightDate(), flight.getFlightTime());
			if (dbRec != null) {
				updatedList.add(dbRec);
			} else {
				updatedList.add(flight);
			}
		}
		return updatedList;
	}

	@Override
	public Flight findById(Integer id) {
		return flightRepository.findById(id).get();
	}

	@Override
	public Flight saveOrUpdate(Flight flight) {
		return saveOrUpdateAll(Arrays.asList(flight)).get(0);
	}

	@Override
	public void delete(Integer id) {
		flightRepository.deleteById(id);
	}

	@Override
	public List<Flight> fetchFlightsForAirlineAndDate(final Integer airlineId, final LocalDate date) {
		List<Flight> flights = new ArrayList<>();
		flightRepository.findByAirlineIdAndDate(airlineId, date).forEach(flights::add);
		return flights;
	}

	@Override
	public List<Flight> fetchFlightsFromSpecificOriginAndDate(final String origin, final LocalDate date) {
		List<Flight> flights = new ArrayList<>();
		flightRepository.findByOriginAndFlightDate(origin, date).forEach(flights::add);
		return flights;
	}

	@Override
	public List<Flight> fetchFlightsFromOriginToDestinationOnDate(final String origin, final String destination,
			final LocalDate date) {
		List<Flight> flights = new ArrayList<>();
		flightRepository.findByOriginAndDestinationAndFlightDate(origin, destination, date).forEach(flights::add);
		return flights;
	}

	@Override
	public List<Flight> fetchSortedFlightsFromOriginToDestinationOnDate(String origin, String destination,
			LocalDate date, Sort sort) {
		List<Flight> flights = new ArrayList<>();
		flightRepository.findSortedRecordsByOriginAndDestinationAndFlightDate(origin, destination, date, sort)
				.forEach(flights::add);
		return flights;
	}

	@Override
	public List<Flight> fetchSortedFlightsFromOriginToDestinationOnDateAndVacantPassenger(String origin,
			String destination, LocalDate date, Integer vacantSeatsRequired) {
		List<Flight> flights = new ArrayList<>();
		flightRepository.findFareSortedRecordsByOriginAndDestinationAndFlightDateAndVacantSeats(origin, destination,
				date, vacantSeatsRequired).forEach(flights::add);
		return flights;
	}

	@Override
	public List<Flight> fetchFlightsFromSpecificFlightNumberOriginAndDestination(final String flightNumber,
			final String origin, final String destination) {
		List<Flight> flights = new ArrayList<>();
		flightRepository.findByFlightNumberAndOriginAndDestination(flightNumber, origin, destination)
				.forEach(flights::add);
		return flights;
	}

	@Override
	public Flight fetchFlightsFromSpecificFlightNumberAndFlightDateAndTime(final String flightNumber,
			final LocalDate flightDate, final LocalTime flightTime) {
		return flightRepository.findByFlightNumberAndFlightDateAndFlightTime(flightNumber, flightDate, flightTime);
	}

}
