package com.admbootup.spring.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.admbootup.spring.entity.FlightInfo;
import com.admbootup.spring.repo.FlightInfoRepository;
import com.admbootup.spring.service.FlightInfoService;

@Service
public class FlightInfoServiceimpl implements FlightInfoService {

	@Autowired
	private FlightInfoRepository flightInfoRepository;

	@Override
	public List<FlightInfo> getAllFlightInfos() {
		List<FlightInfo> flightInfoList = new ArrayList<>();
		flightInfoRepository.findAll().forEach(flightInfoList::add);
		return flightInfoList;
	}

	@Override
	public FlightInfo getByName(String Name) {
		return flightInfoRepository.findByFlightNumber(Name);
	}

	public List<FlightInfo> findAttachedFlightInfo(final Iterable<FlightInfo> flightInfos) {
		List<FlightInfo> flightInfosAttached = new ArrayList<>();
		for (FlightInfo fi : flightInfos) {
			FlightInfo dbRec = flightInfoRepository.findByFlightNumber(fi.getFlightNumber());
			if (dbRec != null)
				flightInfosAttached.add(dbRec);
			else
				flightInfosAttached.add(fi);
		}
		return flightInfosAttached;
	}

	@Override
	public List<FlightInfo> updateOrSaveAll(Iterable<FlightInfo> flightInfo) {
		return flightInfoRepository.saveAll(findAttachedFlightInfo(flightInfo));
	}

	@Override
	public FlightInfo getFlightInfo(final Integer id) {
		return flightInfoRepository.findById(id).get();
	}

	@Override
	public FlightInfo updateOrSave(final FlightInfo flightInfo) {
		return updateOrSaveAll(Arrays.asList(flightInfo)).get(0);
	}

	@Override
	public void delete(final Integer id) {
		flightInfoRepository.deleteById(id);
	}

}
