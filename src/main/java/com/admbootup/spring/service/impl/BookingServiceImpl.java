package com.admbootup.spring.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.admbootup.spring.entity.BookingRecord;
import com.admbootup.spring.entity.Flight;
import com.admbootup.spring.entity.Inventory;
import com.admbootup.spring.entity.Passenger;
import com.admbootup.spring.repo.BookingRepository;
import com.admbootup.spring.repo.InventoryRepository;
import com.admbootup.spring.repo.PassengerRepository;
import com.admbootup.spring.service.BookingService;

@Service
public class BookingServiceImpl implements BookingService {

	@Autowired
	private BookingRepository bookingRepository;
	@Autowired
	private InventoryRepository inventoryRepository;

	@Autowired
	private PassengerRepository passengerRepository;

	@Override
	public BookingRecord bookFlight(final Flight flight, final List<Passenger> passengers) {

		if (CollectionUtils.isEmpty(passengers) || flight == null) {
			throw new RuntimeException("Could not find proper information!");
		}
		BookingRecord booking = new BookingRecord();
		booking.setBookingDate(LocalDateTime.now());
		booking.setDestination(flight.getDestination());
		Double fare = flight.getFare().getFare() * passengers.size();
		booking.setFare(fare);
		booking.setFlightNumber(flight.getFlightNumber());
		booking.setOrigin(flight.getOrigin());

		booking.setStatus("Confirmed");
		booking.setTripDate(flight.getFlightDate());
		booking.setTripTime(flight.getFlightTime());
		booking.setTripDuration(flight.getDuration());

		BookingRecord attached = retrieveExistingBookingForFlight(flight);
		if (attached != null) {
			booking = attached;
		}
		for (Passenger passenger : passengers) {
			if (booking.getPassengers().contains(passenger))
				throw new RuntimeException("Already Present!");
		}

		Inventory inv = flight.getInventory();
		inv.setCount(inv.getCount() - passengers.size());
		inventoryRepository.save(inv);
		booking = bookingRepository.save(booking);
		List<Passenger> attachedPassengers = passengerRepository.findAllById(passengers.stream().map(p -> p.getId()).collect(Collectors.toList()));
		booking.getPassengers().addAll(attachedPassengers);
		return bookingRepository.save(booking);
	}

	private BookingRecord retrieveExistingBookingForFlight(Flight flight) {
		BookingRecord record = bookingRepository.findByFlightNumberAndOriginAndDestinationAndTripDateAndTripTime(
				flight.getFlightNumber(), flight.getOrigin(), flight.getDestination(), flight.getFlightDate(),
				flight.getFlightTime());

		return record;
	}

}
