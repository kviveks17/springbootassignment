package com.admbootup.spring.service;

import java.util.List;

import com.admbootup.spring.entity.Passenger;

public interface PassengerService {

	List<Passenger> findAll();

	List<Passenger> saveOrUpdateAll(final List<Passenger> passengers);

	Passenger saveOrUpdate(final Passenger passenger);

	void deleteById(final Integer passengerId);

	Passenger findById(final Integer passengerId);

	Passenger findByEmailId(final String emailId);
}
