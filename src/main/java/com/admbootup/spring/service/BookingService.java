package com.admbootup.spring.service;

import java.util.List;

import com.admbootup.spring.entity.BookingRecord;
import com.admbootup.spring.entity.Flight;
import com.admbootup.spring.entity.Passenger;

/**
 * Service implementation for handling Booking
 * 
 * @author Singh, Vivek Kumar(CTS-EMP:525662)
 *
 */
public interface BookingService {

	BookingRecord bookFlight(final Flight flight, final List<Passenger> passengers);

}
