package com.admbootup.spring.util;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.admbootup.spring.dto.DashBoardDto;

public class DashBoardUtil {

	public static List<DashBoardDto> convertDataToDto(List<String> dashboardData) {
		List<DashBoardDto> dbDtoList = new ArrayList<>();
		dashboardData.stream().forEach(data -> {
			DashBoardDto dbDto = convertStringToDto(data);
			dbDtoList.add(dbDto);
		});
		return dbDtoList;
	}

	private static DashBoardDto convertStringToDto(String dashBoardData) {
		List<String> dataList = Arrays.asList(dashBoardData.split(","));
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
		DashBoardDto dbDto = new DashBoardDto();
		dbDto.setId(Integer.parseInt(dataList.get(0)));
		dbDto.setDestination(dataList.get(1));
		dbDto.setDuration(dataList.get(2));
		dbDto.setFlightDate(LocalDate.parse(dataList.get(3), dateFormatter));
		dbDto.setFlightNumber(dataList.get(4));
		dbDto.setFlightTime(LocalTime.parse(dataList.get(5), timeFormatter));
		dbDto.setOrigin(dataList.get(6));
		dbDto.setFareId(Integer.parseInt(dataList.get(7)));
		dbDto.setFlightInfoId(Integer.parseInt(dataList.get(8)));
		dbDto.setInvoiceId(Integer.parseInt(dataList.get(9)));
		dbDto.setAirlineId(Integer.parseInt(dataList.get(10)));
		dbDto.setSeatCounts(Integer.parseInt(dataList.get(11)));
		dbDto.setType(dataList.get(12));
		dbDto.setLogo(dataList.get(13));
		dbDto.setName(dataList.get(14));
		return dbDto;
	}
}
