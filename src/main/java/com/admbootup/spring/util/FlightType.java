package com.admbootup.spring.util;

import lombok.Getter;

@Getter
public enum FlightType {

	AIRBUS("Airbus", 150);

	private String name;
	private Integer seatCounts;

	private FlightType(final String name, final Integer seatCounts) {
		this.name = name;
		this.seatCounts = seatCounts;
	}
}
