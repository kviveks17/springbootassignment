package com.admbootup.spring.service;

import static com.admbootup.spring.util.CITIES.DELHI;
import static org.junit.Assert.assertFalse;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.admbootup.spring.entity.AirlineInfo;
import com.admbootup.spring.entity.BookingRecord;
import com.admbootup.spring.entity.Fare;
import com.admbootup.spring.entity.Flight;
import com.admbootup.spring.entity.FlightInfo;
import com.admbootup.spring.entity.Passenger;
import com.admbootup.spring.util.CITIES;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Assignment2Test {
	@Autowired
	private AirlineInfoService airlineInfoService;

	@Autowired
	private FlightService flightService;

	@Autowired
	private FlightInfoService flightInfoService;

	@Autowired
	private PassengerService passengerService;

	@Autowired
	private BookingService bookingService;

	@Test
	public void test1RetrieveIndigoFlightTripsOn21stAug2020() {
		System.out.println(
				"\n======================test1RetrieveIndigoFlightTripsOn21stAug2020 start===========================");
		AirlineInfo aiIndigo = airlineInfoService.getAirlineInfoByName("Indigo");
		List<Flight> flights = flightService.fetchFlightsForAirlineAndDate(aiIndigo.getId(), LocalDate.of(2020, 8, 21));
		flights.forEach(System.out::println);
		System.out.println(
				"======================test1RetrieveIndigoFlightTripsOn21stAug2020 end===========================");
		assertFalse(CollectionUtils.isEmpty(flights));

	}

	@Test
	public void test2RetrieveFlightsLeavingDelhiOn21stAug2020() {
		System.out.println(
				"\n======================test2RetrieveFlightsLeavingDelhiOn21stAug2020 start===========================");
		List<Flight> flights = flightService.fetchFlightsFromSpecificOriginAndDate("DELHI", LocalDate.of(2020, 8, 21));
		flights.forEach(System.out::println);
		System.out.println(
				"======================test2RetrieveFlightsLeavingDelhiOn21stAug2020 end===========================");
		assertFalse(CollectionUtils.isEmpty(flights));

	}

	@Test
	public void test3RetrieveFlightsFromDelhiToChennaiOn21stAug2020() {
		System.out.println(
				"\n======================test3RetrieveFlightsFromDelhiToChennaiOn21stAug2020 start===========================");
		List<Flight> flights = flightService.fetchSortedFlightsFromOriginToDestinationOnDateAndVacantPassenger("DELHI",
				"CHENNAI", LocalDate.of(2020, 8, 21), 4);
		flights.forEach(System.out::println);
		System.out.println(
				"======================test3RetrieveFlightsFromDelhiToChennaiOn21stAug2020 end===========================");
		assertFalse(CollectionUtils.isEmpty(flights));

	}

	@Test
	public void test4RetrieveFlightsWithFlightNumberFromOriginToDestination() {
		System.out.println(
				"\n======================test4RetrieveFlightsWithFlightNumberFromOriginToDestination start===========================");
		List<Flight> flights = flightService.fetchFlightsFromSpecificFlightNumberOriginAndDestination("AI-840",
				CITIES.DELHI.name(), CITIES.CHENNAI.name());
		flights.forEach(System.out::println);
		System.out.println(
				"======================test4RetrieveFlightsWithFlightNumberFromOriginToDestination end===========================");
		assertFalse(CollectionUtils.isEmpty(flights));

	}

	@Test
	public void test5RetrieveFlightsWithFlightNumberAndDateTime() {

		System.out.println(
				"\n======================test5RetrieveFlightsWithFlightNumberAndDateTime start===========================");
		Flight flight = flightService.fetchFlightsFromSpecificFlightNumberAndFlightDateAndTime("AI-840",
				LocalDate.of(2020, 8, 21), LocalTime.of(2, 12));
		System.out.println(flight);
		System.out.println(
				"======================test5RetrieveFlightsWithFlightNumberAndDateTime end===========================");
		assertFalse(flight == null);

	}

	@Test
	public void test6ScheduleAdditionalFlightsFromPuneToChennai() {

		System.out.println(
				"\n======================test6ScheduleAdditionalFlightsFromPuneToChennai start===========================");
		FlightInfo fI5756 = flightInfoService.getByName("I5-756");
		FlightInfo fI5757 = flightInfoService.getByName("I5-757");
		FlightInfo fSG434 = flightInfoService.getByName("SG-434");
		FlightInfo fSG435 = flightInfoService.getByName("SG-435");
		FlightInfo fUK830 = flightInfoService.getByName("UK-830");
		FlightInfo fUK831 = flightInfoService.getByName("UK-831");

		Flight f1 = new Flight(CITIES.CHENNAI.name(), "2 hours 15 mins", LocalDate.of(2020, 8, 22), LocalTime.of(2, 12),
				CITIES.PUNE.name(), new Fare("INR", 4500D), fI5756);

		Flight f2 = new Flight(CITIES.CHENNAI.name(), "2 hours 45 mins", LocalDate.of(2020, 8, 22), LocalTime.of(1, 15),
				CITIES.PUNE.name(), new Fare("INR", 3500D), fI5757);

		Flight f3 = new Flight(CITIES.CHENNAI.name(), "2 hours 50 mins", LocalDate.of(2020, 8, 22), LocalTime.of(2, 30),
				CITIES.PUNE.name(), new Fare("INR", 5000D), fSG434);

		Flight f4 = new Flight(CITIES.CHENNAI.name(), "1 hours 45 mins", LocalDate.of(2020, 8, 22), LocalTime.of(2, 45),
				CITIES.PUNE.name(), new Fare("INR", 2546D), fSG435);

		Flight f5 = new Flight(CITIES.CHENNAI.name(), "3 hours 00 mins", LocalDate.of(2020, 8, 22), LocalTime.of(3, 00),
				CITIES.PUNE.name(), new Fare("INR", 7500D), fUK830);

		Flight f6 = new Flight(CITIES.CHENNAI.name(), "3 hours 15 mins", LocalDate.of(2020, 8, 22), LocalTime.of(3, 15),
				CITIES.PUNE.name(), new Fare("INR", 10000D), fUK831);

		Iterable<Flight> savedFlights = flightService.saveOrUpdateAll(java.util.Arrays.asList(f1, f2, f3, f4, f5, f6));
		savedFlights.forEach(System.out::println);
		System.out.println(
				"======================test6ScheduleAdditionalFlightsFromPuneToChennai end===========================");

	}

	@Test
	public void test7BookFlightFor2PassengerFromDelhiToHyderabad() {
		System.out.println(
				"\n======================test7BookFlightFor2PassengerFromDelhiToHyderabad start===========================");
		FlightInfo goAir = flightInfoService.getByName("G8-428");
		Flight flight = flightService
				.saveOrUpdate(new Flight(DELHI.name(), "3 hours 00 mins", LocalDate.of(2020, 8, 21),
						LocalTime.of(1, 15), CITIES.HYDERABAD.name(), new Fare("INR", 8000D), goAir));
		List<Passenger> passengers = passengerService.findAll();
		BookingRecord record = bookingService.bookFlight(flight, passengers);
		System.out.println(record);
		System.out.println(
				"======================test7BookFlightFor2PassengerFromDelhiToHyderabad end===========================");
	}

	@Test
	public void test8Schedule4FlightFromDelhiToPuneOn21stAug() {
		System.out.println(
				"\n======================test8Schedule4FlightFromDelhiToPuneOn21stAug start===========================");

		FlightInfo fAI870 = flightInfoService.getByName("AI-870");
		FlightInfo f6E6687 = flightInfoService.getByName("6E-6687");
		FlightInfo fUK833 = flightInfoService.getByName("UK-833");
		FlightInfo fI5758 = flightInfoService.getByName("I5-758");

		Flight flight1 = flightService.saveOrUpdate(new Flight(DELHI.name(), "2 hours 15 mins",
				LocalDate.of(2020, 8, 21), LocalTime.of(1, 00), CITIES.PUNE.name(), new Fare("INR", 5000D), fAI870));
		Flight flight2 = flightService.saveOrUpdate(new Flight(DELHI.name(), "2 hours 15 mins",
				LocalDate.of(2020, 8, 21), LocalTime.of(3, 15), CITIES.PUNE.name(), new Fare("INR", 6000D), f6E6687));
		Flight flight3 = flightService.saveOrUpdate(new Flight(DELHI.name(), "2 hours 15 mins",
				LocalDate.of(2020, 8, 21), LocalTime.of(23, 15), CITIES.PUNE.name(), new Fare("INR", 5000D), fUK833));
		Flight flight4 = flightService.saveOrUpdate(new Flight(DELHI.name(), "2 hours 15 mins",
				LocalDate.of(2020, 8, 21), LocalTime.of(5, 35), CITIES.PUNE.name(), new Fare("INR", 4000D), fI5758));

		Iterable<Flight> savedFlights = flightService
				.saveOrUpdateAll(java.util.Arrays.asList(flight1, flight2, flight3, flight4));
		savedFlights.forEach(System.out::println);
		System.out.println(
				"======================test8Schedule4FlightFromDelhiToPuneOn21stAug end===========================");
	}

	@Test
	public void test9UpdateIndigoFlightScheduledOn21AugTo22AugFromDelhiToPune() {
		System.out.println(
				"\n======================test9UpdateIndigoFlightScheduledOn21AugTo22AugFromDelhiToPune start===========================");

		Flight flight1 = flightService.fetchFlightsFromSpecificFlightNumberAndFlightDateAndTime("6E-6686",
				LocalDate.of(2020, 8, 21), LocalTime.of(3, 15));
		System.out.println("Before Rescheduling flight:" + flight1);
		flight1.setFlightDate(LocalDate.of(2020, 8, 22));
		flight1 = flightService.saveOrUpdate(flight1);
		System.out.println("After Rescheduling flight to 22nd Aug 2020:" + flight1);

		System.out.println(
				"======================test9UpdateIndigoFlightScheduledOn21AugTo22AugFromDelhiToPune end===========================");
	}

}
